package com.epam.model;

import com.epam.model.reflection.ReflectionMethod;
import com.epam.model.reflection.ReflectionMyClass;

import java.lang.reflect.Field;
import java.util.List;
import java.util.Map;

public class Logic implements Model {


    @Override
    public void createAnnotation() {
        Class clazz = MyAnnotation.class;
        Field[] fields = clazz.getDeclaredFields();
        for (Field field : fields) {
            if (field.isAnnotationPresent(Name.class)) {
                Name myName = (Name) field.getAnnotation(Name.class);
                String name = myName.name();
                String type = myName.type();
                System.out.println("field: " + field.getName());
                System.out.println("name in class: " + name);
                System.out.println("type in class: " + type);
            }
        }
    }

    @Override
    public void invokeMethods() {
        ReflectionMethod.invokeThreeMethods(new ThreeMethods());
    }

    @Override
    public void createClass() {
        ReflectionMyClass.invokeMyClass(new MyClass());
    }
}
