package com.epam.model.reflection;

import com.epam.model.MyClass;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class ReflectionMyClass {
    public static void invokeMyClass (MyClass myClass) {
        Class<MyClass> clazz = MyClass.class;
        try {
            Method method1 = clazz.getDeclaredMethod("myMethod", String.class, int[].class);
            Method method2 = clazz.getDeclaredMethod("myMethod", String[].class);
            method1.invoke(myClass, "Nikki Lauda", new int[] {2, 0, 0, 4, 2, 0, 1, 9});
            method2.invoke(myClass, new Object[] { new String[] {"Nikki Lauda", "James Hunt"}});
        } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
            e.printStackTrace();
        }
    }
}
